package userManagement;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
//import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.Assert;
import org.testng.annotations.Test;
//import all constants defined under utility package
import utility.Constant;

public class ImportLogin extends Logins{
	public ImportLogin(WebDriver driver) {
		super(driver);
	}
	
	static WebDriver driver = null;
	//manual or active directory login
    @Test(priority=1)
    public void manualLogin() throws IOException, InterruptedException{
		
    	
    	String browser = "Chrome";
    	if(browser == "Chrome")
    	{
    		System.setProperty("webdriver.chrome.driver","C:/Selenium/chromedriver.exe");
    		driver = new ChromeDriver();
    	}
    	else if(browser == "Firefox")
    	{
    		System.setProperty("webdriver.gecko.driver","C:/Selenium/geckodriver.exe");
    		driver = new FirefoxDriver();
    	}
    	else if(browser == "safari"){
    		//System.setProperty("webdriver.safari.driver", "C:/Selenium/SafariDriver.safariextz");
    		driver = new SafariDriver();
    	}
    		driver.get(Constant.URL+"/login");
		//path of the root of this file
    	String filePath = System.getProperty("user.dir");
    	
    	//define xls/xlsx file name
    	String fileName = Constant.IMPORTFILENAME;
    	
    	//define the tab name inside that sheet
    	String sheetName=Constant.MANUALLOGINSHEET;
    	
	    //Create an object of File class to open xlsx file
	    File file =    new File(filePath+"\\"+fileName);

	    //Create an object of FileInputStream class to read excel file
	    FileInputStream inputStream = new FileInputStream(file);

	    Workbook fileInputStreamObj = null;

	    //Find the file extension by splitting file name in substring  and getting only extension name
	    String fileExtensionName = fileName.substring(fileName.indexOf("."));

	    //Check condition if the file is xlsx file
	    if(fileExtensionName.equals(".xlsx")){

	    //If it is xlsx file then create object of XSSFWorkbook class
	    	fileInputStreamObj = new XSSFWorkbook(inputStream);
	    }

	    //Check condition if the file is xls file
	    else if(fileExtensionName.equals(".xls")){

	        //If it is xls file then create object of XSSFWorkbook class
	    	fileInputStreamObj = new HSSFWorkbook(inputStream);
	    }

	    //Read sheet inside the workbook by its name
	    Sheet loginSheet = fileInputStreamObj.getSheet(sheetName);
	    
	    //Find number of rows in excel file
	    int rowCount = loginSheet.getLastRowNum()-loginSheet.getFirstRowNum();
	    
	    //Create a loop over all the rows of excel file to read it
	    for (int i = 1; i < rowCount+1; i++) {

	        Row row = loginSheet.getRow(i);
	        
	        //Open URL
        	//driver.get(Constant.LIVEURL+"/login");
    		
        	//get the email field and set the value of first column from current row in it
        	WebElement email = driver.findElement(By.name("email"));
    		String emailValue = row.getCell(0).getStringCellValue();
    		email.sendKeys(emailValue);
    		
    		//get the email field and set the value of first column from current row in it
    		WebElement password = driver.findElement(By.name("password"));
    		String passwordValue = row.getCell(1).getStringCellValue();
    		password.sendKeys(passwordValue);
    		
    		WebElement submit = driver.findElement(By.name("login-submit"));
    		submit.click();
    		
    		//wait after logged in
    		Thread.sleep(6000);
    		Boolean result = null;
    		
    		//maximize the window as sometime it may not find the menu at bottom
   		    driver.manage().window().maximize();
   		    //Check if Logout link exists in the page to verify successful login
    		result = driver.getPageSource().contains("Logout");
    		
    		//Check if test case pass or not
    		//Assert.assertEquals(true,result);
    		//Value of result should be true
    		Assert.assertFalse(result);
    		//if pass then logout the first window
    		WebElement logout = driver.findElement(By.linkText("Logout"));
    		logout.click();
 
	    }
   }
    
    @Test(priority=2)
    public void googleLogin() throws IOException, InterruptedException{
		
    	
		//path of the root of this file
    	String filePath = System.getProperty("user.dir");
    	
    	//define xls/xlsx file name
    	String fileName = Constant.IMPORTFILENAME;
    	
    	//define the tab name inside that sheet
    	String sheetName=Constant.GOOGLELOGINSHEET;
    	
	    //Create an object of File class to open xlsx file
	    File file =    new File(filePath+"\\"+fileName);

	    //Create an object of FileInputStream class to read excel file
	    FileInputStream inputStream = new FileInputStream(file);

	    Workbook fileInputStreamObj = null;

	    //Find the file extension by splitting file name in substring  and getting only extension name
	    String fileExtensionName = fileName.substring(fileName.indexOf("."));

	    //Check condition if the file is xlsx file
	    if(fileExtensionName.equals(".xlsx")){

	    //If it is xlsx file then create object of XSSFWorkbook class
	    	fileInputStreamObj = new XSSFWorkbook(inputStream);
	    }
	    //Check condition if the file is xls file
	    else if(fileExtensionName.equals(".xls")){

	        //If it is xls file then create object of XSSFWorkbook class
	    	fileInputStreamObj = new HSSFWorkbook(inputStream);
	    }

	    //Read sheet inside the workbook by its name
	    Sheet loginSheet = fileInputStreamObj.getSheet(sheetName);
	    
	    //Find number of rows in excel file
	    int rowCount = loginSheet.getLastRowNum()-loginSheet.getFirstRowNum();
	    
	    //Create a loop over all the rows of excel file to read it
	    for (int i = 1; i < rowCount+1; i++) {

	        Row row = loginSheet.getRow(i);
	        
	        //Open URL
        	driver.get(Constant.URL+"/login");
    		WebElement glogin = driver.findElement(By.xpath(".//*[@id='login-form']/div[5]/div[1]/a[1]/img"));
    		glogin.click();
    		
    		Thread.sleep(4000);
    		
    		//on next page hit "Sign In with Google"
    		WebElement signInGmail =driver.findElement(By.className("btn-login"));
    		signInGmail.click();
    		Thread.sleep(4000);
    		
        	//get the email field and set the value of first column from current row in it
    		WebElement email = driver.findElement(By.id("identifierId"));
    		String emailValue =row.getCell(0).getStringCellValue();
    		email.sendKeys(emailValue);
    		
    		//hit next button
    		WebElement submit = driver.findElement(By.id("identifierNext"));
    		submit.click();
    		
    		//wait after logged in
    		Thread.sleep(7000);
    		Boolean result = null;
    		
    		//get the email field and set the value of first column from current row in it
    		WebElement password = driver.findElement(By.name("password"));
    		String passwordValue = row.getCell(1).getStringCellValue();
    		password.sendKeys(passwordValue);
    		
    		//hit next button
    		WebElement next = driver.findElement(By.cssSelector("#passwordNext content span"));
    		next.click();
    		Thread.sleep(6000);
   		    //Check if Logout link exists in the page to verify successful login
    		result = driver.getPageSource().contains("Logout");
    		
    		//Check if test case pass or not
    		//Value of result should be true
    		Assert.assertFalse(result);
    		
    		//if pass then logout the first window
    		WebElement logout = driver.findElement(By.linkText("Logout"));
    		logout.click();
	    }
   }   
}
