package userManagement;
import static org.testng.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import userManagement.Logins;
import utility.Constant;
import userManagement.Pass;

//As class Pass already extends Logins then no need to inherit Logins again
public class QuickPass extends Rooms{
	
	static SoftAssert softAssert = new SoftAssert();
	static Logins login = new Logins(); 
	//Rooms rm = new Rooms();
	//static String reasonDescription = "Just want to go to washroom. No reason to specify here. Just a test";
	
	public static void create() throws InterruptedException {
		
		Thread.sleep(3000);
		
		Logins.manualLogin(Constant.STUDENTEMAIL,Constant.STUDENTPASSWORD);
		driver.manage().window().maximize();
		
		WebElement createPassMenu =  driver.findElement(By.linkText("Create Pass"));
		createPassMenu.click();
		Thread.sleep(3000);
		
		//send the text to the auto suggest box
		WebElement teacher = driver.findElement(By.name("fromteacher1"));
		//teacher.sendKeys("JanetTeac1LN");
		teacher.sendKeys("Janet");
		//wait for sometime to auto populate the values
		Thread.sleep(6000);
		
		//select the first value in auto suggest drop-down
		WebElement searchResult = driver.findElement(By.cssSelector("div.fsearchres a.list-group-item span"));
		searchResult.click();
		
		Thread.sleep(3000);
		//we will use the same value as in Pass class (which we use in creating pass type)
		WebElement wantToGo = driver.findElement(By.linkText(Pass.passType ));//we can pass this from Rooms class
		wantToGo.click();
		
		Thread.sleep(4000);
		//Select room = new Select(driver.findElement(By.cssSelector("div.active .selectroom")));//before the css changes by developers
		Select room = new Select(driver.findElement(By.cssSelector(".to-block .tab-content .active .selectroom")));
		room.selectByIndex(1);
		//room.selectByValue(Constant.ROOMNAME);
		WebElement reason = driver.findElement(By.cssSelector(".to-block .tab-content .active textarea"));
		
		reason.sendKeys(Constant.reasonDescription);
		
		WebElement submit = driver.findElement(By.id("bttbutton"));
		submit.submit();
	}
	@Test(priority=5)
	public static void checkCreatePass() throws InterruptedException {
		
		create();
		Thread.sleep(2000);
		
		/*One way to check test result is check the URL it redirected to
		 * String currentURL = driver.getCurrentUrl();
		String expectedURL = Constant.LIVEURL+"/epass";
		assertEquals(currentURL, expectedURL);*/
		Boolean actual = false;
		
		if(driver.getPageSource().contains(Constant.reasonDescription))
		{
			actual = true;
		}
		assertEquals(actual, (Boolean)true);
		
	}
	@Test(dependsOnMethods = "checkCreatePass",priority=6)
	public void cancelQuickPass() throws InterruptedException {
		
		driver.findElement(By.cssSelector("div.card-head a")).click();
		Thread.sleep(5000);
		
		//hit the yes button on the modal box
		WebElement cancelButton =  driver.findElement(By.id("ajaxcansubmint"));
		cancelButton.click();
		Thread.sleep(4000);
		
		String passClass  = driver.findElement(By.cssSelector("span#currentpass div")).getAttribute("class");
		//if cancelled pass background is grey(match the class name), then script pass
		assertEquals(passClass,"student-pass_card card-wrapper card-wrapper-2 ");
		
	}
	@Test(priority=7)
	public void teacherApproval() throws InterruptedException {
		
		create();
		Thread.sleep(2000);
		
		//hit the from button in teacher block
		WebElement teacherPin =  driver.findElement(By.cssSelector("div.teacher-block span"));
		teacherPin.click();
		Thread.sleep(2000);
		//WebElement oneWaySign = driver.findElement(By.cssSelector("div.modal-body .form-group"))
		WebElement checkBox = driver.findElement(By.xpath("//input[@name = 'optradio' and @type = 'checkbox' and @value = '1']")); 
		checkBox.click();
		//set teacher pin
		WebElement teacherPinNumber =  driver.findElement(By.id("txtpin"));
		teacherPinNumber.sendKeys("3333");
		//write some note
		WebElement teacherNote =  driver.findElement(By.id("note"));
		teacherNote.sendKeys("This is just a test note from the teacher");
		//hit submit button
		WebElement submit =  driver.findElement(By.className("btn-primary"));
		submit.click();
		Thread.sleep(3000);
		
		//if approved pass background is green(match the class name), then script pass
		String  classNamePrint =  driver.findElement(By.cssSelector("span#currentpass div")).getAttribute("class");
		
		softAssert.assertEquals(classNamePrint,"student-pass_card card-wrapper pass-sucess ");
		softAssert.assertAll();
	}
	@Test(priority=9,dependsOnMethods="teacherApproval")
	public void teacherEndPass() throws InterruptedException {

		Thread.sleep(3000);
		//WebElement toLink = driver.findElement(By.cssSelector(".pass-sucess a .teacher-block span"));
		WebElement toLink = driver.findElement(By.xpath(".//*[@id='currentpass']/div/a[2]/div/span"));
		toLink.click();
		
		Thread.sleep(5000);
		WebElement pin = driver.findElement(By.id("txtpin"));
		pin.sendKeys(Constant.TEACHERPIN);
		
		WebElement submit = driver.findElement(By.id("ajaxsubmint"));
		submit.click();
		Thread.sleep(3000);
		//if pass ends then  background is grey(match the class name), then script pass
		String  classNamePrint =  driver.findElement(By.cssSelector("span#currentpass div")).getAttribute("class");
		softAssert.assertEquals(classNamePrint,"student-pass_card card-wrapper card-wrapper-2 ");
		softAssert.assertAll();
	}
}
