package userManagement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Test;

import utility.Constant;
public class Pass extends Logins{
	
	static Logins login = new Logins();
	static String passType = Constant.PASSTYPENAME;
	@Test(priority=1)
	public void createPassType() throws InterruptedException {
		Logins.manualLogin(Constant.ADMINEMAIL, Constant.ADMINPASSWORD);
		
		driver.manage().window().maximize();
		//get and hit the pass setting menu on left sidebar 
		WebElement passSetting = driver.findElement(By.linkText("Pass Setting"));
		passSetting.click();
		
		Thread.sleep(2000);
		//hit the pass tabs menu
		WebElement passTabs = driver.findElement(By.linkText("Pass Tabs"));
		passTabs.click();
		Thread.sleep(2000);
		
		//get and hit the Create button
		WebElement create = driver.findElement(By.className("btn-primary"));
		create.click();
		
		/*PASS TYPE SETTING*/
		WebElement typeName = driver.findElement(By.id("name"));
		typeName.sendKeys(passType);
		
		WebElement description = driver.findElement(By.id("discription"));
		description.sendKeys("This is just a test pass. this pass is for bathroom");
		
		WebElement order = driver.findElement(By.id("order"));
		order.sendKeys("1");
		
		Select receiver = new Select (driver.findElement(By.id("receiver")));
		receiver.selectByValue("1");
		
		Select comment = new Select(driver.findElement(By.id("comment")));
		comment.selectByValue("1");
		
		Select status = new Select(driver.findElement(By.id("status")));
		status.selectByVisibleText("ACTIVE");
		
		WebElement submit = driver.findElement(By.className("btn-success"));
		submit.submit();
		Thread.sleep(2000);
		//if redirected to the pass tab manage page then test pass
		String redirectedUrl = driver.getCurrentUrl();
		String expectedUrl = Constant.URL+"/passtypes";
		Assert.assertEquals(redirectedUrl, expectedUrl);
		/*End*/
		
	}
	@Test(priority=2)
	public void updatePassType() throws InterruptedException {
		
		//hit the pass tabs menu
		WebElement passTabs =  driver.findElement(By.linkText("Pass Tabs"));
		passTabs.click();
		Thread.sleep(2000);
		
		//hit the edit button of first item
		WebElement edit = driver.findElement(By.linkText("Edit"));
		edit.click();
		
		Thread.sleep(2000);
		WebElement typeName = driver.findElement(By.id("name"));
		typeName.clear();
		typeName.sendKeys(Pass.passType);
		
		WebElement desc = driver.findElement(By.id("discription"));
		desc.clear();
		desc.sendKeys("Testng secobd times");
		
		WebElement order = driver.findElement(By.id("order"));
		order.clear();
		order.sendKeys("2");
		
		Select status = new Select(driver.findElement(By.id("status")));
		status.selectByValue("1");
		
		WebElement submit = driver.findElement(By.className("btn-success"));
		submit.submit();System.out.println("updatePassType 3");
		//if redirected to the pass tab manage page then test pass
		String redirectedUrl = driver.getCurrentUrl();
		String expectedUrl = Constant.URL+"/passtypes";
		Assert.assertEquals(redirectedUrl, expectedUrl);
		
	}

}
