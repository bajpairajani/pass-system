package userManagement;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

//import all constants defined under utility package
import utility.Constant;

public class Logins {
	//static WebDriver driver;
    protected final WebDriver driver;
    
    public Logins(WebDriver driver) {
        this.driver = driver;

      //WebDriver driver = null;
      		String browser ="chrome";
      		
      		if(browser == "chrome") {
      			System.setProperty("webdriver.chrome.driver",Constant.CHROMEDRIVER);
      			driver = new ChromeDriver();
      		}
      		else if(browser == "firefox") {
      			System.setProperty("webdriver.gecko.driver", Constant.FIREFOXDRIVER);
      			driver = new FirefoxDriver();
      		}
      		
      		driver.get(Constant.URL+"/login");
      		WebElement email = driver.findElement(By.name("email"));
      		email.sendKeys(Constant.STUDENTEMAIL);
      		
      		WebElement password = driver.findElement(By.name("password"));
      		password.sendKeys(Constant.STUDENTPASSWORD);
      		
      		WebElement submit = driver.findElement(By.name("login-submit"));
      		submit.click();
    }

	/*public static void manualLogin(String userEmail, String userPassword) {
		//WebDriver driver = null;
		String browser ="chrome";
		
		if(browser == "chrome") {
			System.setProperty("webdriver.chrome.driver",Constant.CHROMEDRIVER);
			driver = new ChromeDriver();
		}
		else if(browser == "firefox") {
			System.setProperty("webdriver.gecko.driver", Constant.FIREFOXDRIVER);
			driver = new FirefoxDriver();
		}
		
		driver.get(Constant.URL+"/login");
		WebElement email = driver.findElement(By.name("email"));
		email.sendKeys(userEmail);
		
		WebElement password = driver.findElement(By.name("password"));
		password.sendKeys(userPassword);
		
		WebElement submit = driver.findElement(By.name("login-submit"));
		submit.click();
	}*/

}
