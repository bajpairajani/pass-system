package utility;

public class Constant {
	//pass name and room name in alphabetic order
	//name should be initial caps
	public static final String URL = "https://www.e-hallpass.com";
	//public static final String URL = "http://staging.e-hallpass.com";
	
	public static final String STUDENTEMAIL    	= "janet.rajani+14@ithands.net";
	public static final String STUDENTPASSWORD 	= "123456";
	
	public static final String TEACHEREMAIL 	= "janet.rajani+16@ithands.net";
	public static final String TEACHERPASSWORD 	= "123456";
	
	public static final String ADMINEMAIL 		= "janet.rajani+18@ithands.net";
	public static final String ADMINPASSWORD 	= "123456";
	
	public static final String IMPORTFILENAME 	= "ImportLoginDataLive.xlsx";
	
	//Credentials for staging site 
	/*public static final String STUDENTEMAIL    	= "janetStu1@ithands.com";
	public static final String STUDENTPASSWORD 	= "password";
	
	public static final String TEACHEREMAIL 	= "janetTeac1@ithands.com";
	public static final String TEACHERPASSWORD 	= "password";
	
	public static final String ADMINEMAIL 		= "janetAdmin1@ithands.com";
	public static final String ADMINPASSWORD 	= "password";
	public static final String IMPORTFILENAME 	= "ImportLoginDataStaging.xlsx";*/
	//End staging credentials
	
	public static final String SUADMINEMAIL 	= "suadmin@eduspire.org";
	public static final String SUADMINPASSWORD 	= "ehallpass#123";
	
	//public static final String SUBTEACHEREMAIL 	= "janetSubs@cysd.k12.pa.us";
	
	//this email is used when we generate sub teacher from super admin
	public static final String SUBTEACHEREMAIL 	= "janetSubs@ithtest1.com";
	public static final String GOOGLELOGINSHEET = "gmailLogin";
	public static final String MANUALLOGINSHEET = "manualLogin";
	
	public static final String ROOMNAME = "Zoom One";
	public static final String PASSTYPENAME = "Zoom Add Pass";
	
	public static final String reasonDescription = "Just want to go to washroom. No reason to specify here. Just a test";
	//public static final String LIVEURL         	= "https://www.e-hallpass.com";
	//public static final String STAGINGURL      	= "http://staging.e-hallpass.com";
	
	//ITH1 teachers pin
	public static final String TEACHERPIN  		= "3333";
	//name visible in time zone drop-down
	public static final String TIMEZONE 		= "US/Central";
	
	public static final String CHROMEDRIVER 	= "/Users/janetrajani/eclipse-workspace/chromedriver";
	public static final String FIREFOXDRIVER 	= "C:/Selenium/gecko.exe";
	public static final String IMPORTUSERCSV 	= "usersImport.csv";
}
