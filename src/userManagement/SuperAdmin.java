package userManagement;
import userManagement.QuickPass;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import utility.Constant;
public class SuperAdmin{
	
	//static WebDriver  driver;
	//static String subTeacherPin;
	static SoftAssert softAssert = new SoftAssert();
	static WebDriver dr = QuickPass.driver;
	public void login() throws InterruptedException {
		
		String browser ="Chrome";
		if(browser == "Chrome") {
			System.setProperty("webdriver.chrome.driver", Constant.CHROMEDRIVER);
			dr = new ChromeDriver();
		}
		else {
			System.setProperty("webdriver.gecko.driver", Constant.FIREFOXDRIVER);
			dr = new FirefoxDriver();
		}
		
		dr.get(Constant.URL+"/suadmin");
		WebElement username =  dr.findElement(By.name("email"));
		username.sendKeys(Constant.SUADMINEMAIL);
		
		WebElement password = dr.findElement(By.name("password"));
		password.sendKeys(Constant.SUADMINPASSWORD);
		
		WebElement submit  = dr.findElement(By.className("btn-default"));
		submit.submit();
		
		Thread.sleep(3000);
		
		//String superAdminText = driver.findElement(By.cssSelector("ul#sidemenu li .image a span strong h2.user-name")).getText();
		//softAssert.assertEquals(superAdminText,"Super Admin");
		//softAssert.assertAll();
		
	}
	@Test
	public void ChangeTimeZone() throws InterruptedException {
		//login to super admin
		login();
		List<WebElement> allSchoolLinks =  dr.findElements(By.cssSelector("div#schoolListTable_wrapper table#schoolListTable tbody tr td a.btn-primary"));
		outerloop:
		for(WebElement e:allSchoolLinks) {
			String schoolLink = e.getAttribute("href");
			
			if(schoolLink.contains("/suadmin/edit/school/45")) {
				
				System.out.println(schoolLink);
				//you can get the text on which this attribute is applied with this
				e.click();
				//if we will not come out of the for loop here then it will throw "Stale element error"
				//this happens because we redirect to another page before the loop completes
				break outerloop;
			}
		}
		
		/*change time zone*/
		Thread.sleep(6000);
		Select timezone = new Select(dr.findElement(By.id("timeZone")));
		timezone.selectByValue(Constant.TIMEZONE);
		
		WebElement submit = dr.findElement(By.xpath("//input[@value = 'Submit']"));
		submit.click();
		/*End time zone change*/
		String successMessage =  dr.findElement(By.cssSelector(".page-content-wrapper .alert-success ul li")).getText();
		
		Assert.assertTrue(successMessage.contains("successfully updated"));
	}
	
	
}
