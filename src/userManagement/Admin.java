package userManagement;

import org.testng.asserts.SoftAssert;
import au.com.bytecode.opencsv.CSVReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;
import userManagement.QuickPass;
import utility.Constant;
import userManagement.Logins;

public class Admin{
	static SoftAssert softAssert = new SoftAssert();
	
	public void verifyTimeZone() throws InterruptedException {
		QuickPass.create();
		Thread.sleep(5000);
		
		//hit the from button in teacher block
		WebElement teacherPin =  QuickPass.driver.findElement(By.cssSelector(".card-wrapper div.teacher-block span .fa-check"));
		teacherPin.click();
		
		Thread.sleep(4000);
		WebElement checkBox = QuickPass.driver.findElement(By.xpath("//input[@name = 'optradio' and @type = 'checkbox' and @value = '1']")); 
		checkBox.click();
		//set teacher pin
		WebElement teacherPinNumber =  QuickPass.driver.findElement(By.id("txtpin"));
		teacherPinNumber.sendKeys(Constant.TEACHERPIN);
		
		//write some note
		WebElement teacherNote =  QuickPass.driver.findElement(By.id("note"));
		teacherNote.sendKeys("Note for testing the timezone");
		//hit submit button
		WebElement submit =  QuickPass.driver.findElement(By.className("btn-primary"));
		submit.click();
		Thread.sleep(6000);
		
		//if approved pass background is green(match the class name), then script pass
		String  timeOfPassOut =  QuickPass.driver.findElement(By.xpath(".//*[@id='currentpass']/div/div[3]/div/table/tbody/tr[1]/td[2]")).getText();
		System.out.println("time set on quick pass is "+timeOfPassOut);
		
		String realTime = timezone(Constant.TIMEZONE);
		System.out.println("current time with new timezone is "+realTime);
		
		//if there is 1 min difference between both times, ignore it else throw error
		if ((timeOfPassOut.compareTo(realTime) > 1) || (timeOfPassOut.compareTo(realTime) < 1) || (timeOfPassOut.compareTo(realTime) == 0)) {
            System.out.println("There is either 1 min difference or the time is exact same");
            assert(true);
        }else {
            System.out.println("Time difference is bigger than 1 min between current time of the changed timezone and the quick pass time");
            assert(false);
        }
	}
	
	public  String timezone(String timeZoneFormat) {
		
		TimeZone timeZone = TimeZone.getTimeZone(timeZoneFormat);
		  
		String timeFormat = "hh:mm";
		//String dateFormat = "MMMM dd,yyyy G"; 
		DateFormat dateFormat = new SimpleDateFormat(timeFormat);
		// Setting the Timezone 
		Calendar cal = Calendar.getInstance(timeZone);
		dateFormat.setTimeZone(cal.getTimeZone());
		// Picking the time value in the required Format 
		String currentTime = dateFormat.format(cal.getTime());
		  
		/*
		Date todayDate = new Date();
		// Specifying the format 
		DateFormat todayDateFormat = new SimpleDateFormat("MMMM dd,yyyy");
		// Setting the Timezone 
		todayDateFormat.setTimeZone(timeZone);
		// Picking the date value in the required Format 
		String strTodayDate = todayDateFormat.format(todayDate);
		System.out.println("Current date:" + strTodayDate);*/
		
		return currentTime;
	}
	@Test(groups = {"pass_time_out"})
	public void passTimeSetting() throws InterruptedException {
		//Logins driver = Logins.driver;
		Logins.manualLogin(Constant.ADMINEMAIL, Constant.ADMINPASSWORD);
		Logins.driver.manage().window().maximize();
		Thread.sleep(10000);
		//hit the Pass Setting menu item
		//WebElement passSettingLink = driver.findElement(By.className("fa-cogs"));
		WebElement passSettingLink = Logins.driver.findElement(By.linkText("Pass Setting"));
		passSettingLink.click();
		Thread.sleep(2000);
		//hit the pass times menu item
		WebElement passTimes = Logins.driver.findElement(By.linkText("Pass Times"));
		passTimes.click();
		
		/*change the pass timing for automation testing purpose*/
		WebElement awaitingApproval =  Logins.driver.findElement(By.id("white_pass"));
		awaitingApproval.sendKeys("");
		awaitingApproval.sendKeys("00:01:00");
		Thread.sleep(3000);
		WebElement longRunningPass =  Logins.driver.findElement(By.id("min_time"));
		longRunningPass.sendKeys("");
		longRunningPass.sendKeys("00:02:00");
		Thread.sleep(3000);
		WebElement systemEndedPass =  Logins.driver.findElement(By.id("auto_expire"));
		systemEndedPass.sendKeys("");
		systemEndedPass.sendKeys("00:03:00");
		Thread.sleep(3000);
		WebElement extendedPassTime =  Logins.driver.findElement(By.id("extended_pass"));
		extendedPassTime.sendKeys("");
		extendedPassTime.sendKeys("01:04:00");
		//Press tab to shift focus to submit button
		extendedPassTime.sendKeys(Keys.TAB);
		/*End pass time change*/
		
		WebElement submit = Logins.driver.findElement(By.className("btn-primary"));
		submit.submit();
		Thread.sleep(2000);
		//WebElement passSetting = Logins.driver.findElement(By.linkText("Pass Setting"));
		//passSetting.click();
		
		Boolean expected = true;
		Boolean successMessage =  Logins.driver.findElement(By.cssSelector("div.content section.content div.box div.box-body div")).getAttribute("class").contains("alert-success");
		softAssert.assertEquals(successMessage, expected);
		//WebElement logout = Logins.driver.findElement(By.linkText("Logout"));
		//logout.click();
		
		
		proxyPass();
		Thread.sleep(6000);
		
		/*Check if pass is Active Pass by checking color and the value "Active" in the row*/
		String green  = Logins.driver.findElement(By.cssSelector("table#myTablePass tbody tr.pass-row td")).getCssValue("background-color");
		softAssert.assertEquals(green, "rgba(204, 255, 204, 1)"); //#ccffcc
		
		String checkGreen  = Logins.driver.findElement(By.cssSelector("table#myTablePass tbody tr td a span.text-muted")).getText();
		softAssert.assertEquals(checkGreen, "Active");
		/*End green pass color*/
		Thread.sleep(180000);
		/*Check if pass is yellow Active Pass by checking color and the value "Active" in the row*/
		String yellow  = Logins.driver.findElement(By.cssSelector("table#myTablePass tbody tr.pass-row td")).getCssValue("background-color");
		
		softAssert.assertEquals(yellow, "rgba(254, 255, 178, 1)");//#feffb2
		
		String checkYellow  = Logins.driver.findElement(By.cssSelector("table#myTablePass tbody tr td a span.text-muted")).getText();
		softAssert.assertEquals(checkYellow, "Active");
		/*End yellow pass color*/
		
		Thread.sleep(210000);
		/*check if pass ends automatically*/ 
		int  passGone  = Logins.driver.findElements(By.cssSelector("table#myTablePass tbody tr")).size();
		//if no pass row exists now then the test will pass
		//softAssert.assertFalse(passGone>0);//this will throw false in assert all 
		
		softAssert.assertEquals(passGone, 0);
		//softAssert.assertTrue(passGone==0);
		System.out.println("passGone"+passGone);
		/*End pass out*/
		softAssert.assertAll();
		
	}
	public void proxyPass() throws InterruptedException {
		Logins.manualLogin(Constant.TEACHEREMAIL, Constant.TEACHERPASSWORD);
		Thread.sleep(8000);
		
		WebElement ProxyPass = Logins.driver.findElement(By.cssSelector("ul.sidebar-menu li.treeview .fa-plus-circle"));
		ProxyPass.click();
		
		//click on the drop-down
		WebElement studentDropDown = Logins.driver.findElement(By.cssSelector("div.searchteacher1 span.select2-container"));
		studentDropDown.click();
		
		//send the text to the auto suggest box
		WebElement student = Logins.driver.findElement(By.className("select2-search__field"));
		
		student.sendKeys("Janet");
		//wait for sometime to auto populate the values
		Thread.sleep(6000);
		
		//select the first value in auto suggest drop-down
		//WebElement searchResult = Logins.driver.findElement(By.cssSelector("div.fsearchres a.list-group-item span"));//this not working now but it was working earlier
		WebElement searchResult = Logins.driver.findElement(By.cssSelector(".select2-container--open .select2-dropdown .select2-results"));
		searchResult.click();
		
		Thread.sleep(3000);
		//we will use the same value as in Pass class (which we use in creating pass type)
		WebElement wantToGo = Logins.driver.findElement(By.linkText(Pass.passType ));//we can pass this from Rooms class
		wantToGo.click();
		
		Thread.sleep(4000);
		//Select room = new Select(driver.findElement(By.cssSelector("div.active .selectroom")));//before the css changes by developers
		Select room = new Select(Logins.driver.findElement(By.cssSelector(".to-block .tab-content .active .selectroom")));
		room.selectByIndex(1);
		//room.selectByValue(Constant.ROOMNAME);//this not working now but it was working earlier
		WebElement reason = Logins.driver.findElement(By.cssSelector(".to-block .tab-content .active textarea"));
		
		reason.sendKeys(Constant.reasonDescription);
		
		WebElement submit = Logins.driver.findElement(By.id("bttbutton"));
		submit.submit();
	}
	public void importUsersCsv() throws InterruptedException, IOException{
		//Logins driver = Logins.driver;
		Logins.manualLogin(Constant.ADMINEMAIL, Constant.ADMINPASSWORD);
		Logins.driver.manage().window().maximize();
		Thread.sleep(10000);
		//hit the user management menu item
		WebElement passSettingLink = Logins.driver.findElement(By.linkText("User Management"));
		passSettingLink.click();
		Thread.sleep(2000);
		//hit the Main Users menu item
		WebElement passTimes = Logins.driver.findElement(By.linkText("Main Users"));
		passTimes.click();
		Thread.sleep(2000);
		//Hit the import users from csv button
		WebElement importUserCsvButton = Logins.driver.findElement(By.linkText("Import Users From Csv File"));
		importUserCsvButton.click();
		Thread.sleep(5000);
		WebElement uploadInputBox = Logins.driver.findElement(By.name("csvImport"));
		
		String filePath = System.getProperty("user.dir");
		uploadInputBox.sendKeys(filePath+"\\"+Constant.IMPORTUSERCSV);
		//uploadInputBox.sendKeys("C:/Selenium/EhallPassScript/usersImport.csv");
		
		Thread.sleep(4000);
		//WebElement hiddenUploadInputBox = Logins.driver.findElement(By.name("_token"));
		//hiddenUploadInputBox.sendKeys("C:/Selenium/EhallPassScript/usersImport.csv");
		//Thread.sleep(2000);
		//hit choose file button
		WebElement uploadButton = Logins.driver.findElement(By.id("finalSubmit"));
		uploadButton.click();
		Thread.sleep(2000);
		
		WebElement popupNextButton = Logins.driver.findElement(By.id("utlbtn"));
		popupNextButton.click();
		
		Thread.sleep(2000);
		
		WebElement popupNextButtonAgain = Logins.driver.findElement(By.id("utlbtn"));
		popupNextButtonAgain.click();
		Thread.sleep(2000);
		WebElement updateUsersButton = Logins.driver.findElement(By.id("updateUserRec"));
		updateUsersButton.click();
		Thread.sleep(2000);
		WebElement importEntriesButton = Logins.driver.findElement(By.id("finalcsvtodata"));
		importEntriesButton.click();
		
		Thread.sleep(6000);
		//String checkNewEntries = Logins.driver.findElement(By.className("userAct")).getText();
		//readCSV();
		
	}
	
	@Test(groups = {"import_users"})
	public void readCSV() throws IOException, InterruptedException {
		
		/*import users from CSV*/
		importUsersCsv();
		/*End user import*/
		Thread.sleep(10000);
		// This will load csv file 
		String filePath = System.getProperty("user.dir");
		CSVReader reader = new CSVReader(new FileReader(filePath+"\\"+Constant.IMPORTUSERCSV));
		
		// this will load content into list
		List<String[]> li=reader.readAll();
		System.out.println("Total rows which we have is "+li.size());
		// create Iterator reference
		Iterator<String[]>i1= li.iterator();
		// Iterate all values 
		 while(i1.hasNext()){
				
			 String[] str=i1.next();
			 Boolean importedUserResult = null;
			 Boolean expectedResult = true;
			 
			 for(int i=0;i<str.length;i++)
			 {
				if(str[i].equals("firstname") || str[i].equals("lastname") || str[i].equals("username") || str[i].equals("email") || str[i].equals("role") 
				    || str[i].equals("status") || str[i].equals("gradyear") || str[i].equals("studentNumber") || str[i].equals("BuildingCode")) {
					System.out.println(" in if value = "+str[i]);
				}
				else {
					System.out.println(" in else value = "+str[i]);
				//check if first name exists
				importedUserResult = Logins.driver.getPageSource().contains(str[0]);
				softAssert.assertEquals(expectedResult, importedUserResult);
				
				//check if last name exists
				importedUserResult = Logins.driver.getPageSource().contains(str[1]);
				softAssert.assertEquals(expectedResult, importedUserResult);
				
				//check if email exists
				importedUserResult = Logins.driver.getPageSource().contains(str[2]);
				softAssert.assertEquals(expectedResult, importedUserResult);
				//check all the test passes
				softAssert.assertAll();
				}
			 }
		}
	}
}
