package userManagement;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Test;
//import all constants defined under utility package
import utility.Constant;

public class UpdateProfile extends Logins{

	public UpdateProfile(WebDriver driver) {
		super(driver);
	}

	@Test(priority=3)
	public void updateAdminProfile() throws InterruptedException
	{
		//send parameters to the function

		Thread.sleep(3000);
		
        //hit the my profile link
        WebElement editProfile = driver.findElement(By.partialLinkText("My Profile"));
        editProfile.click();
        
        //browse logo
        WebElement logoPath = driver.findElement(By.name("upload_image"));
        //set the path of your local drive
        logoPath.sendKeys("C:/Selenium/logo.png");
        
        //set user title
        Select title = new Select(driver.findElement(By.name("initials")));
        title.selectByVisibleText("Miss.");
        
        //set first name
        WebElement firstName = driver.findElement(By.name("firstname"));
        firstName.clear();
        firstName.sendKeys("Janet");
        
        //set last name
        WebElement lastName = driver.findElement(By.name("lastname"));
        lastName.clear();
        lastName.sendKeys("Rajani");
        
        //set the searchable priority to "No"
        WebElement searchable =driver.findElement(By.cssSelector("input[value='1']"));
        searchable.click();
       
        //set pin 
        WebElement teacherPin = driver.findElement(By.name("teacher_pin"));
        teacherPin.clear();
        teacherPin.sendKeys("3333");
        
        //hit submit button
        WebElement update = driver.findElement(By.cssSelector("div.box-footer button.btn-success"));
        update.submit();
        Thread.sleep(18000);
        //Get the success message
		String successMessage =driver.findElement(By.className("alert-success")).getText();
		
        /*Boolean actualResult=false;
		if(successMessage !="") {
			actualResult = true;
		}
		//Check if test case pass or not
		Assert.assertEquals(true,actualResult);*/
		Assert.assertFalse(successMessage =="");
	}
	
	@Test(priority=2)
	public void updateTeacherProfile() throws InterruptedException
	{
		//send parameters to the function
		//Logins.manualLogin(Constant.TEACHEREMAIL,Constant.TEACHERPASSWORD);
		Thread.sleep(3000);
		
        //hit the my profile link
        WebElement editProfile = driver.findElement(By.partialLinkText("My Profile"));
        editProfile.click();
        
        //browse logo
        WebElement logoPath = driver.findElement(By.name("upload_image"));
        //set the path of your local drive
        logoPath.sendKeys("C:/Selenium/logo.png");
        
        //set user title
        Select title = new Select(driver.findElement(By.name("initials")));
        title.selectByVisibleText("Miss.");
        
        //set first name
        WebElement firstName = driver.findElement(By.name("firstname"));
        firstName.clear();
        firstName.sendKeys("Rajani");
        
        //set password
        WebElement password = driver.findElement(By.name("password"));
        password.sendKeys("password");
        
        //set the searchable priority to "No"
        WebElement searchable =driver.findElement(By.cssSelector("input[value='1']"));
        searchable.click();
       
        //set pin 
        WebElement teacherPin = driver.findElement(By.name("teacher_pin"));
        teacherPin.clear();
        teacherPin.sendKeys("4444");
        
        //hit submit button
        WebElement save = driver.findElement(By.cssSelector("div.box-footer button.btn-success"));
        save.submit();
        
        Thread.sleep(18000);
        //Get the success message
		String successMessage =driver.findElement(By.className("alert-success")).getText();
		
        /*Boolean actualResult=false;
		if(successMessage !="") {
			actualResult = true;
		}*/
		//Check if test case pass or not
		//Assert.assertEquals(true,actualResult);
		Assert.assertFalse(successMessage =="");
		
	}
	
	@Test(priority=1)
	public void updateUserProfile() throws InterruptedException
	{
		//send parameters to the function
		Thread.sleep(3000);
		
        //hit the my profile link
        WebElement editProfile = driver.findElement(By.partialLinkText("Profile"));
        editProfile.click();
    
        //set first name
        WebElement phone = driver.findElement(By.name("phone"));
        phone.clear();
        phone.sendKeys("8886315665");
        
        //select carrier
        Select carrier = new Select(driver.findElement(By.id("carrier")));
        carrier.selectByVisibleText("General Communications Inc.");
        
        //choose option for text message reminder
        WebElement receiveSMS =driver.findElement(By.xpath("//input[@name='status' and @value='0']"));
        receiveSMS.click();
        
        //hit submit button
        WebElement save = driver.findElement(By.className("btn-primary"));
        save.click();
        Thread.sleep(18000);
        
        //Boolean actualResult=false;
        //Get the success message
		String successMessage =driver.findElement(By.className("alert-success")).getText();
		/*if(successMessage !="") {
			actualResult = true;
		}*/
		//Check if test case pass or not
		//Assert.assertEquals(true,actualResult);
		Assert.assertFalse(successMessage =="");
		
	}
}