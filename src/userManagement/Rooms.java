package userManagement;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import utility.Constant;
import userManagement.Pass;

public class Rooms extends Pass{
	static Logins login =new Logins();
	//You can also define result like this from addRoom and use this in editRoom
	//private int mResult;
	String roomNameValue = Constant.ROOMNAME;
	SoftAssert softAssert = new SoftAssert();

	@Test(priority=3)
	public void addRoom() throws InterruptedException {
		WebElement roomLink = driver.findElement(By.linkText("Rooms"));
		roomLink.click();
		//hit Create button to add new room
		Thread.sleep(4000);
		WebElement createRoom = driver.findElement(By.linkText("Create"));
		createRoom.click();
		Thread.sleep(2000);
		Select typeName =new Select(driver.findElement(By.name("room_type")));
		typeName.selectByVisibleText(Pass.passType);
		
		//select pass type
		Select passType = new Select(driver.findElement(By.name("two_way")));
		passType.selectByVisibleText("1 Signature Pass");
		Thread.sleep(2000);
		//add room name
		WebElement roomName = driver.findElement(By.name("room_name"));
		roomName.sendKeys(roomNameValue);
		
		//set the room status as active
		Select status = new Select(driver.findElement(By.name("status")));
		status.selectByValue("1");
		
		//hit submit button
		WebElement submit = driver.findElement(By.className("btn-success"));
		submit.submit();
		System.out.println("addRoom");
		//successful submission will redirect to the manage rooms page
		String currentURL = driver.getCurrentUrl();
		String expectedUrl = Constant.URL+"/rooms";
		//Check if the URL is matched with the manage rooms URL
		//Assert.assertEquals(expectedUrl,currentURL);
		//this will make sure that even on error it will not stop the test script. But it actually doesn't report error
		softAssert.assertEquals(currentURL, expectedUrl);
		//this will make sure errors are reported properly
		softAssert.assertAll();
		
	}
	
	@Test(priority=4)
	public void editRoom() throws InterruptedException {
		//Hit Rooms sub-menu
		WebElement roomLink = driver.findElement(By.linkText("Rooms"));
		roomLink.click();
		
		WebElement editRoom = driver.findElement(By.linkText("Edit"));
		editRoom.click();
		Thread.sleep(4000);
		
		Select typeName = new Select(driver.findElement(By.name("room_type")));
		typeName.selectByVisibleText(Pass.passType);
		Thread.sleep(2000);
		//Pass type
		Select twoWay = new Select(driver.findElement(By.name("two_way")));
		twoWay.selectByVisibleText("2 Signature Pass");
		
		//Room name
		WebElement roomName = driver.findElement(By.name("room_name"));
		roomName.clear();
		roomName.sendKeys(roomNameValue);
		
		//set status
		Select status = new Select(driver.findElement(By.name("status")));
		status.selectByVisibleText("ACTIVE");
		
		//hit submit button
		WebElement submit = driver.findElement(By.className("btn-success"));
		submit.click();
		System.out.println("editRoom 3");
		//successful submission will redirect to the manage rooms page
		String currentURL = driver.getCurrentUrl();
		String expectedUrl = Constant.URL+"/rooms";
		//Check if the URL is matched with the manage rooms URL
		Assert.assertEquals(expectedUrl,currentURL);
	
	}
	
	//delete room after quick pass script
	/*@Test(priority=10)
	public void deleteRoom() throws InterruptedException {
		login.manualLogin(Constant.ADMINEMAIL, Constant.ADMINPASSWORD);
		Thread.sleep(3000);
		//Hit Pass setting menu
		WebElement passSetting =  driver.findElement(By.linkText("Pass Setting"));
		passSetting.click();
		Thread.sleep(2000);
		
		//Hit Rooms sub-menu
		WebElement roomLink = driver.findElement(By.linkText("Rooms"));
		roomLink.click();
		Boolean actualResult= true;
		
		//get the first delete button link
		WebElement deleteButtonLink = driver.findElement(By.className("btn-danger"));
		//save the deleted link in a string for later match
		String deletedLink = deleteButtonLink.getAttribute("href");
		
		//Hit delete button
		deleteButtonLink.click();
		
		//hit the "Ok" button on delete confirmation box
		Alert alert = driver.switchTo().alert();
		alert.accept();
		
		//Get all delete button links after deleting the first
		List<WebElement> myElements = driver.findElements(By.className("btn-danger"));
		//WebElement query_enquirymode = myElements.get(2); // this will get the second element in the array
		for(WebElement e : myElements) {
			
			//get delete button link
			String linksOneByOne = e.getAttribute("href");
			
			//Now check if the deleted link still exists
			if(deletedLink==linksOneByOne){
				//if link still exist then test fail
				actualResult = false;
			}
			else{
				//If the deleted button link doesn't exist then test pass
				actualResult = true;
			}
		}
		//Check if the URL is matched with the manage rooms URL
		Assert.assertEquals(true,actualResult);
	}*/
}