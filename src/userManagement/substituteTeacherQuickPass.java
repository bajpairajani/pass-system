package userManagement;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;
import userManagement.Logins;
import utility.Constant;

//As class Pass already extends Logins then no need to inherit Logins again
public class substituteTeacherQuickPass extends SuperAdmin{
	
	//QuickPass quickPass = new QuickPass();
	static Logins login = new Logins(); 
	static String reasonDescription = "Just want to go to washroom. No reason to specify here. Just a test";
	/*@DataProvider(name="pinDataProvider")
	public Object[][] dataProvider(){
		//object array can hold int, char, string, float, boolean etc all data types in Java
		return new Object[][] {{"k1","k2"}};
	}
	@Test(dataProvider="pinDataProvider")
	public void show(String str1, String str2) throws InterruptedException {
		
		System.out.println(str1 + " " + str2);
	}*/
	
	@Test(priority=1)
	public void addSubstituteTeacher() throws InterruptedException {
		login();
		//this xpath will also work but the element position might change when we add more schools in system
		//List<WebElement> allSchoolLinks =  dr.findElements(By.xpath(".//*[@id='schoolListTable']/tbody/tr[21]/td[3]/a"));
		List<WebElement> allSchoolLinks =  dr.findElements(By.cssSelector("div#schoolListTable_wrapper table#schoolListTable tbody tr td a.btn-primary"));
		forouterloop:
		for(WebElement e:allSchoolLinks) {
			String schoolLink = e.getAttribute("href");
			
			if(schoolLink.contains("/45/45")) {
				//you can get the text on which this attribute is applied with this
				e.click();
				break forouterloop;
			}
		}
		//hit the substitute link
		WebElement substituteLink = dr.findElement(By.linkText("Substitute Users"));
		substituteLink.click();
		Thread.sleep(3000);
		
		WebElement addSubstituteButton = dr.findElement(By.partialLinkText("Add Substitute Teacher"));
		addSubstituteButton.click();
		Thread.sleep(3000);
		
		/*add substitute details*/
		Select title = new Select(dr.findElement(By.id("initials")));
		title.selectByValue("Miss.");
		
		WebElement firstName = dr.findElement(By.id("firstname"));
		firstName.sendKeys("Rajani");
		
		WebElement lastname = dr.findElement(By.id("lastname"));
		lastname.sendKeys("Janet");
		
		WebElement email = dr.findElement(By.id("email"));
		email.sendKeys(Constant.SUBTEACHEREMAIL);
		//generate auto pin
		WebElement generatePinButton = dr.findElement(By.className("updatepin"));
		generatePinButton.click();
		Thread.sleep(3000);
		WebElement updateButton = dr.findElement(By.className("btn-primary"));
		updateButton.submit();
		
		Thread.sleep(3000);
		/*End*/
		
		/*Check after saving substitute info the success message popup*/
		String successMessageBox = dr.findElement(By.cssSelector("section.content div.box-default div.box-body div")).getAttribute("class");
		Boolean actualResult = false;
		if(successMessageBox.contains("alert-success")) {
			actualResult = true;
		}
		softAssert.assertEquals(actualResult,true);
		/*End*/
		
		/*Check if the update button takes user to substitute teacher page*/
		String currentURL = dr.getCurrentUrl();
		String expectedURL = Constant.URL+"/substituteteacher";
		Boolean URLResult = false;
		
		if(currentURL==expectedURL) {
			URLResult = true;
		}
		softAssert.assertEquals(URLResult,true);
		/*End*/
		//softAssert.assertAll();
	}
	
	@Test(priority=2)
	public void editSubstituteTeacher() throws InterruptedException {
		
		//edit first substitute entry
		WebElement editSubstitute = dr.findElement(By.className("fa-edit"));
		editSubstitute.click();
		Thread.sleep(3000);
		
		/*add substitute details*/
		Select title = new Select(dr.findElement(By.id("initials")));
		title.selectByValue("Dr.");
		
		WebElement firstName = dr.findElement(By.id("firstname"));
		firstName.clear();
		firstName.sendKeys("Rajani");
		
		WebElement lastname = dr.findElement(By.id("lastname"));
		lastname.clear();
		lastname.sendKeys("Bajpai");
		
		WebElement email = dr.findElement(By.id("email"));
		email.clear();
		email.sendKeys(Constant.SUBTEACHEREMAIL);
		
		WebElement generatePinButton = dr.findElement(By.className("updatepin"));
		generatePinButton.click();
		Thread.sleep(2000);
		
		//send the pin to another method
		String subTeacherPin = dr.findElement(By.id("teacher_pin")).getAttribute("value");
		/*Manage cookies*/
		Cookie cookie = new Cookie("SubTeacherPin",subTeacherPin);
		dr.manage().addCookie(cookie);
		
		/*End manage cookies*/
		Thread.sleep(3000);
		
		WebElement updateButton = dr.findElement(By.className("btn-primary"));
		updateButton.submit();
		
		Thread.sleep(3000);
		/*End*/
		
		/*Check after saving substitute info the success message popup*/
		String successMessageBox = dr.findElement(By.cssSelector("section.content div.box-default div.box-body div")).getAttribute("class");
		Boolean actualResult = false;
		if(successMessageBox.contains("alert-success")) {
			actualResult = true;
		}
		softAssert.assertEquals(actualResult, true);
		/*End*/
		
		/*Check if the update button takes user to substitute teacher page*/
		String currentURL = dr.getCurrentUrl();
		
		Boolean URLResult = false;
		if(currentURL==Constant.URL+"/substituteteacher") {
			URLResult = true;
		}
		softAssert.assertEquals(URLResult,true);
		/*End*/
		//softAssert.assertAll();
	}
	public static void create() throws InterruptedException {
		
		Thread.sleep(3000);
		
		Logins.manualLogin(Constant.STUDENTEMAIL,Constant.STUDENTPASSWORD);
		
		Thread.sleep(4000);
		WebElement createPassMenu =  Logins.driver.findElement(By.partialLinkText("Create Pass"));
		createPassMenu.click();
		Thread.sleep(3000);
		
		//send the text to the auto suggest box
		WebElement teacher = Logins.driver.findElement(By.name("fromteacher1"));
		teacher.sendKeys("JanetTeac1LN");
		//wait for sometime to auto populate the values
		Thread.sleep(4000);
		
		//select the first value in auto suggest drop-down
		WebElement searchResult = Logins.driver.findElement(By.cssSelector("div.fsearchres a.list-group-item span"));
		searchResult.click();
		
		Thread.sleep(3000);
		//we will use the same value as in Pass class (which we use in creating pass type)
		WebElement wantToGo = Logins.driver.findElement(By.linkText(Pass.passType ));//we can pass this from Rooms class
		wantToGo.click();
		
		Thread.sleep(5000);
		Select room = new Select(Logins.driver.findElement(By.className("selectroom")));
		room.selectByIndex(1);
		
		WebElement reason = Logins.driver.findElement(By.cssSelector("form#frmDashboard textarea"));
		
		reason.sendKeys(reasonDescription);
		
		WebElement submit = Logins.driver.findElement(By.id("bttbutton"));
		submit.submit();
		
	}	
	//test if pin works fine for substitute teacher
	@Test(priority=3)
	public void teacherEnd() throws InterruptedException {
		
		create();
		Thread.sleep(2000);
		//get cookies value
		Cookie subTeachPin = dr.manage().getCookieNamed("SubTeacherPin");
		String pin = subTeachPin.getValue();
				
		//hit the from button in teacher block
		WebElement teacherPin =  Logins.driver.findElement(By.cssSelector("div.teacher-block span"));
		teacherPin.click();
		Thread.sleep(2000);
		
		WebElement checkBox = Logins.driver.findElement(By.xpath("//input[@name = 'optradio' and @type = 'checkbox' and @value = '4']")); 
		checkBox.click();
		//set teacher pin
		WebElement teacherPinNumber =  Logins.driver.findElement(By.id("txtpin"));
		teacherPinNumber.sendKeys(pin);
		
		//write some note
		WebElement teacherNote =  Logins.driver.findElement(By.id("note"));
		teacherNote.sendKeys("This is just a test note from the teacher");
		//hit submit button
		WebElement submit =  Logins.driver.findElement(By.className("btn-primary"));
		submit.click();
		Thread.sleep(3000);
		
		//if approved pass background is green(match the class name), then script pass
		String  classNamePrint =  Logins.driver.findElement(By.cssSelector("span#currentpass div")).getAttribute("class");
		
		softAssert.assertEquals(classNamePrint,"student-pass_card card-wrapper pass-sucess ");
		//softAssert.assertAll();
	}
}

