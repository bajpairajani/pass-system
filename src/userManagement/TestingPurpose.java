package userManagement;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.Test;

import utility.Constant;

public class TestingPurpose {

	static WebDriver driver;
	@Test
	public static void manualLogin() {
		//WebDriver driver = null;
		String browser ="safari";
		
		if(browser == "chrome") {
			System.setProperty("webdriver.chrome.driver",Constant.CHROMEDRIVER);
			driver = new ChromeDriver();
		}
		else if(browser == "firefox") {
			System.setProperty("webdriver.gecko.driver", Constant.FIREFOXDRIVER);
			driver = new FirefoxDriver();
		}
		else if(browser == "safari"){
    		System.setProperty("webdriver.safari.driver", "C:/Selenium/SafariDriver.safariextz");
    		driver = new SafariDriver();
    	}
		
		driver.get(Constant.URL+"/login");
		WebElement email = driver.findElement(By.name("email"));
		email.sendKeys("janetStu1@ithtest1.com");
		
		WebElement password = driver.findElement(By.name("password"));
		password.sendKeys("password");
		
		WebElement submit = driver.findElement(By.name("login-submit"));
		submit.click();
	}
}
